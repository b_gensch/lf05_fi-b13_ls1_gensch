/**
        *   Aufgabe:  Recherechieren Sie im Internet !
        *
        *   Sie dürfen nicht die Namen der Variablen verändern !!!
        *
        *   Vergessen Sie nicht den richtigen Datentyp !!
        *
        *
        * @version 1.1 from 23.09.2021
        * @author Benjamin Gensch
 */

public class WeltDerZahlen {

    public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        int anzahlPlaneten = 8;

        // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 150000000000L;

        // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3645000;

        // Wie alt bist du?  Wie viele Tage sind das?
        int alterTage = 7573;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        int gewichtKilogramm = 200000;

        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        int flaecheGroessteLand = 17098242;

        // Wie groß ist das kleinste Land der Erde?
        float flaecheKleinsteLand = 0.44F;

        /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

        *********************************************************** */
        System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
        System.out.println("Anzahl der Sterne: "+ anzahlSterne);
        System.out.println("Anzahl Einwohner in Berlin: " + bewohnerBerlin);
        System.out.println("Mein Alter in Tagen: " + alterTage);
        System.out.println("Gewicht des schwersten Tieres: " + gewichtKilogramm);
        System.out.println("Fläche des größten Landes: " + flaecheGroessteLand);
        System.out.println("Fläche des kleinsten Landes: " + flaecheKleinsteLand);
        System.out.println(" *******  Ende des Programms  ******* ");

    }
}
