/**
 * Die Klasse beinhaltet die Hauptmethode, mit der man das Spiel "Star Trek" laufen lassen kann.
 * Diese Aufgabe wurde auf Expertenniveau gelöst.
 */
public class StarTrek {

    /**
     * Hauptmethode, um Spiel ablaufen zu lassen.
     * @param args beinhaltet command-line Argumente als Array von String Objekten
     */
    public static void main(String[] args) {

        /*A4.1.1
        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");

        klingonen.addLadung("Ferengi Schneckensaft", 200);
        klingonen.addLadung("Bat'leth Klingonen Schwer", 200);

        romulaner.addLadung("Borg-Schrott", 5);
        romulaner.addLadung("Rote Materie", 2);
        romulaner.addLadung("Plasma-Waffe", 50);

        vulkanier.addLadung("Forschungssonde", 35);
        vulkanier.addLadung("Photonentorpedo", 3);
        */

        //A4.2.1
        Raumschiff klingonen = new Raumschiff(0, 100, 50, 100, 100, 2, "IKS Hegh'ta");
        Raumschiff romulaner = new Raumschiff(2, 50, 50, 100, 100, 2, "IRW Khazara");
        int e = (int) (Math.random() * ((100) + 1));
        int s = (int) (Math.random() * ((100) + 1));
        int sh = (int) (Math.random() * ((100) + 1));
        Raumschiff vulkanier = new Raumschiff(3, e, s, sh, 100, 5, "Ni'Var");

        klingonen.addLadung("Ferengi Schneckensaft", 200);
        klingonen.addLadung("Bat'leth Klingonen Schwer", 200);

        romulaner.addLadung("Borg-Schrott", 5);
        romulaner.addLadung("Rote Materie", 2);
        romulaner.addLadung("Plasma-Waffe", 50);

        vulkanier.addLadung("Forschungssonde", 35);
        vulkanier.addLadung("Photonentorpedo", 3);

        klingonen.shootPT(romulaner);
        romulaner.shootPH(klingonen);
        vulkanier.msgToAll("Gewalt ist nicht logisch");
        klingonen.statusR();
        vulkanier.repair(true, true, true, 5);
        vulkanier.loadPT(3);
        klingonen.shootPT(romulaner);
        klingonen.shootPT(romulaner);
        klingonen.statusR();
        romulaner.statusR();
        vulkanier.statusR();
        System.out.println(Raumschiff.getLog());
    }
}
