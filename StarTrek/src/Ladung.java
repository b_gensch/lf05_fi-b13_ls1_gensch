/**
 * Die Klasse gibt die Struktur eines Objektes "Ladung" vor.
 */
public class Ladung {
    private String name;
    private int anz;


    /**
     * Standard Konstruktor
     */
    public Ladung() {
    }

    /**
     * Konstruktor, um spezifisches Objekt der Klasse Ladung mit vorgegeben Attributen zu erstellen.
     * @param name Name der Ladung
     * @param anz Menge der ladung
     */
    public Ladung(String name, int anz) {
        this.name = name;
        this.anz = anz;
    }

    public String getName() {
        return name;
    }

    public int getAnz() {
        return anz;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnz(int anz) {
        this.anz = anz;
    }

    /**
     * Überschreibt die Standard toString() Methode, um Objekte der Klasse Ladung übersichtlicher auf der Konsole
     * darzustellen.
     * @return gibt alle Attribute des Objektes der Klasse Ladung zurück, von der aus die Methode aufgerufen wurde
     */
    @Override
    public String toString() {
        return "Ladung {" +
                "Bezeichnung='" + name + '\'' +
                ", Menge=" + anz +
                '}';
    }
}
