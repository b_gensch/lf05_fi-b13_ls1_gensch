import java.util.ArrayList;

/**
 * Die Klasse gibt die Struktur eines Objektes "Raumschiff" vor.
 */
public class Raumschiff {
    private int ptAnz, energyP, shieldP, shellP, lifeP, andAnz;
    private String name;
    private static ArrayList<String> broadcastComm;
    private ArrayList<Ladung> loadDirectory;

    /**
     * Standard Konstruktor
     */
    public Raumschiff() {
    }

    /**
     * Konstruktor, um Objekt von Klasse Raumschiff mit Angabe der Parameter zu erstellen.
     * @param pAnz Anzahl der Photonentorpedos
     * @param eP Energieversorgung in Prozent
     * @param sP Schilde in Prozent
     * @param shP Hülle in Prozent
     * @param lP Lebenserhaltungssysteme in Prozent
     * @param aAnz Anzahl von Reparatur Androiden
     * @param n Name des Raumschiffes
     */
    public Raumschiff(int pAnz, int eP, int sP, int shP, int lP, int aAnz, String n) {
        this.ptAnz = pAnz;
        this.energyP = eP;
        this.shieldP = sP;
        this.shellP = shP;
        this.lifeP = lP;
        this.andAnz = aAnz;
        this.name = n;

        broadcastComm = new ArrayList<>();
        loadDirectory = new ArrayList<>();
    }


    public int getPtAnz() {
        return ptAnz;
    }

    public int getEnergyP() {
        return energyP;
    }

    public int getShieldP() {
        return shieldP;
    }

    public int getShellP() {
        return shellP;
    }

    public int getLifeP() {
        return lifeP;
    }

    public int getAndAnz() {
        return andAnz;
    }

    public String getName() {
        return name;
    }

    public void setPtAnz(int pANeu) {
        this.ptAnz = pANeu;
    }

    public void setEnergyP(int ePNeu) {
        this.energyP = ePNeu;
    }

    public void setShieldP(int sPNeu) {
        this.shieldP = sPNeu;
    }

    public void setShellP(int shPNeu) {
        this.shellP = shPNeu;
    }

    public void setLifeP(int lPNeu) {
        this.lifeP = lPNeu;
    }

    public void setAndAnz(int aANeu) {
        this.andAnz = aANeu;
    }

    public void setName(String nNeu) {
        this.name = nNeu;
    }

    /**
     * Fügt ein Objekt der Klasse Ladung zur ArrayList loadDirectory, also dem Ladungsverzeichnis, hinzu.
     * @param n Name der Ladung
     * @param a Menge der Ladung
     */
    public void addLadung(String n, int a) {
        Ladung nL = new Ladung(n, a);
        loadDirectory.add(nL);
        System.out.println("Es wurde folgende Ladung aufgenommen: " + nL);
    }

    /**
     * Schießt einen mit loadPT geladenen Photonentorpedo auf ein beliebiges Raumschiff.
     * Zeigt entsprechende Nachricht an, ob ein Photonentorpedo abgeschossen wurde, oder keiner nachgeladen ist.
     * @param r Raumschiff, auf welches geschossen werden soll
     */
    public void shootPT(Raumschiff r) {
        if (ptAnz == 0) {
            System.out.println("Keine Photonentorpedos gefunden!");
            msgToAll("-=*Click*=-");
        } else {
            ptAnz = ptAnz - 1;
            msgToAll("Photonentorpedo abgeschossen");
            treffer(r);
        }
    }

    /**
     * Schießt einen Schuss der Phaserkanone auf ein beliebiges Raumschiff.
     * Zeigt Nachricht an, ob geschossen wurde, oder nicht genug Energie für Schuss zur Verfügung steht.
     * @param r Raumschiff, auf welches geschossen werden soll
     */
    public void shootPH(Raumschiff r) {
        if (energyP < 50) {
            msgToAll("-=*Click*=-");
        } else {
            energyP = energyP - 50;
            msgToAll("Phaserkanone abgeschossen");
            treffer(r);
        }
    }

    /**
     * Berechnet Parameter des Raumschiffes r neu, da es von einem Schuss getroffen wurde.
     * Schild wird verringert, wenn das Schild bei 0 ist, wird Hülle und Energieversorgung verringert, wenn die Hülle
     * 0 ist, wird der Wert der Lebenserhaltungssysteme verringert.
     * @param r Raumschiff, das getroffen wurde
     */
    private void treffer(Raumschiff r) {
        System.out.println(r.getName() + " wurde getroffen!");
        r.setShieldP(r.getShieldP() - 50);
        if (r.getShieldP() == 0) {
            r.setShellP(r.getShellP() - 50);
            r.setEnergyP(r.getShellP() - 50);
        }
        if (r.getShellP() == 0) {
            r.setLifeP(0);
            msgToAll("Lebenserhaltungssysteme des Raumschiffes " + r.getName() + " vollsändig zerstört.");
        }
    }

    /**
     * Schickt eine Nachricht an "globalen" Broadcast Kommunikator.
     * @param msg Nachricht als String, die verschickt werden soll
     */
    public void msgToAll(String msg) {
        System.out.println(msg);
        broadcastComm.add(msg);
    }

    /**
     * Gibt alle Einträge des "globalen" Broadcast Kommunikators zurück.
     * @return ArrayList Broadcast Kommunikator
     */
    public static ArrayList<String> getLog() {
        return broadcastComm;
    }

    /**
     * Lädt geladene Photonentorpedos aus Ladungsverzeichnis in die Waffe des Raumschiffes nach.
     * @param anzPT Anzahl der zu ladenden Photonentorpedos
     */
    public void loadPT(int anzPT) {
        int lAnzPT = 0;
        String p = "Photonentorpedo";

        for (Ladung l : loadDirectory) {
            if (l.getName().equals(p)) {
                lAnzPT = l.getAnz();
            }
        }
        if (anzPT > lAnzPT) {
            loadDirectory.removeIf(item -> item.getName().equals(p));
            this.setPtAnz(lAnzPT);
        } else {
            for (Ladung l : loadDirectory) {
                if (l.getName().equals(p)) {
                    l.setAnz(lAnzPT - ptAnz);
                }
                this.setPtAnz(ptAnz);
                l.setAnz(lAnzPT - ptAnz);
            }
        }

        System.out.println("-=*Photonentorpedos geladen*=-");
    }

    /**
     * Repariert das Raumschiff mit Reparatur Androiden. Es wird bestimmt, welche Bestandteile des Raumschiffes repariert
     * werden sollen. Die Parameter des zu reparierenden Schiffes werden um den Wert r erhöht, der aus einer Zufallszahl,
     * der Anzahl der Reparatur Androiden und der Anzahl der zu reparierenden Bestandteile errechnet wird.
     * @param shield true, wenn Schild repariert werden soll
     * @param e true, wenn Energieversorgung repariert werden soll
     * @param shell true, wenn Hülle repariert werden soll
     * @param anzD Anzahl der Reparatur Androiden für die Reparatur
     */
    public void repair(boolean shield, boolean e, boolean shell, int anzD) {
        int s = 0;
        if (shield) {
            s = s + 1;
        }
        if (e) {
            s = s + 1;
        }
        if (shell) {
            s = s + 1;
        }

        int x = (int) (Math.random() * ((100) + 1));
        if (anzD > this.getAndAnz()) {
            anzD = this.getAndAnz();
        }
        int r = x * anzD / s;

        if (shield) {
            this.setShieldP(this.getShieldP() + r);
        }
        if (shell) {
            this.setShellP(this.getShellP() + r);
        }
        if (e) {
            this.setEnergyP(this.getEnergyP() + r);
        }

        System.out.println("-=*Raumschiff repariert*=-");
    }

    /**
     * Gibt alle Attribute des Objektes vom Typ Raumschiff aus, von dem aus diese Methode aufgerufen wird.
     */
    public void statusR() {
        System.out.println(this);
    }

    /**
     * Gibt das Ladungsverzeichnis auf die Konsole aus.
     */
    public void getLoadDirectory() {
        System.out.println("Ladungsverzeichnis:" + loadDirectory);
    }

    /**
     * Löscht das gesamte Ladungsverzeichnis.
     */
    public void clearLoadDirectory() {
        loadDirectory.removeIf(item -> item.getAnz() == 0);
        System.out.println("-=*Ladungen gelöscht*=-");
    }

    /**
     * Überschreibt die Standard toString() Methode, um ein Objekt der Klasse Raumschiff besser auf der Konsole
     * darzustellen.
     * @return gibt alle Attribute des Raumschiffes, von dem aus diese Methode aufgerufen wird, zurück
     */
    @Override
    public String toString() {
        return this.getName() + " {" + "ptAnz=" + ptAnz + ", energyP=" + energyP + ", shieldP=" + shieldP + ", shellP=" + shellP + ", lifeP=" + lifeP + ", andAnz=" + andAnz + ", name='" + name + '\'' + ", broadcastComm=" + broadcastComm + ", loadDirectory=" + loadDirectory + '}';
    }
}
