import java.util.Scanner; // Import der Klasse Scanner

public class Rechner {

    // Hier startet das Programm
    public static void main(String[] args) {

        // Neues Scanner-Objekt myScanner wird erstellt
        Scanner myScanner = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

        // Die Variable zahl1 speichert die erste Eingabe
        int zahl1 = myScanner.nextInt();

        System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

        // Die Variable zahl2 speichert die zweite Eingabe
        int zahl2 = myScanner.nextInt();

        // Die Addition der Variablen zahl1 und zahl2
        // wird der Variable ergebnis zugewiesen.
        int ergebnis = zahl1 + zahl2;

        /* Die Subtraktion der Variablen zahl1 und zahl2
        wird der Variable ergebnisSub zugewiesen.
         */
        int ergebnisSub = zahl1 - zahl2;

        /* Die Multiplikation der Variablen zahl1 und zahl2
        wird der Variable ergebnisM zugewiesen.
         */
        int ergebnisM = zahl1 * zahl2;

        /* Die Division der Variablen zahl1 und zahl2
        wird der Variable ergebnisD zugewisen
         */
        int ergebnisD = zahl1 / zahl2;

        System.out.println("\nErgebnis der Addition lautet: ");
        System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
        System.out.println("\nErgebnis der Subtraktion lautet: ");
        System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnisSub);
        System.out.println("\nErgebnis der Multiplikation lautet: ");
        System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnisM);
        System.out.println("\nErgebnis der Division lautet: ");
        System.out.println(zahl1 + " / " + zahl2 + " = " + ergebnisD);

        myScanner.close();

    }
}