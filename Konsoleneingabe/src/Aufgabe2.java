import java.util.Scanner;

public class Aufgabe2 {

    public static void main (String[] args) {
        Scanner in = new Scanner(System.in);
        int alter;
        String name;

        System.out.println("Guten Tag Benutzer.\nBitte geben Sie Ihren Namen ein:");
        name = in.next();
        System.out.println("Nun brauche ich noch Ihr Alter:");
        alter = in.nextInt();
        System.out.println("Ihr Name ist: " + name + "\nIhr Alter ist: " + alter);
        in.close();
    }
}
