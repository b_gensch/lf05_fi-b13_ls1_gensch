import java.util.Scanner;

public class Fahrkartenautomat {
    /* Programmieraufgabe A2.5:
     * Ich habe mich dazu entschieden, der Variable anzTickets den Typ byte zu geben.
     * Wenn jemand sich Fahrkarten kauft, kann er nur ganze Fahrkarten kaufen, keine negative Anzahl und wird
     * wahrscheinlich nicht mehr als 127 Tickets kaufen.
     * Durch die Verwendung von byte wird außerdem etwas weniger Speicher belegt, als zum Beispiel bei int (3 Byte weniger).
     * Ich multipliziere die Anzahl der Tickets mit dem zu zahlenden Betrag und übergebe diesen Wert wieder an die Variable
     * zuZahlenderBetrag zurück.
     */

    private static Scanner tastatur = new Scanner(System.in);

    public static double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag;
        double anzTickets;

        /*
        System.out.println("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        if (zuZahlenderBetrag < 0) {
            System.out.println("Sie können keine negativen Ticketpreise angeben. Der Wert wird ins positive umgewandelt.");
            zuZahlenderBetrag = -zuZahlenderBetrag;
            System.out.println("Zu zahlender Betrag (EURO): " + zuZahlenderBetrag);
        }

        //Anzahl der Tickets
        System.out.println("Anzahl der Tickets: ");
        anzTickets = tastatur.nextByte();
        if (anzTickets > 10) {
            System.out.println("Es können nicht mehr als 10 Tickets bestellt werden.\n Es wird ein Ticket bestellt.");
            anzTickets = 1;
        }
        zuZahlenderBetrag = zuZahlenderBetrag * anzTickets;
        */
        
        /*
        double er = 2.90;
        double tr = 8.60;
        double ktr = 23.50;
        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("    Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("    Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("    Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
        int f = tastatur.nextByte();
        if (f > 3) {
            System.out.println(">>falsche Eingabe<<");
            fahrkartenbestellungErfassen();
        }
        System.out.println("Anzahl der Tickets: ");
        anzTickets = tastatur.nextByte();

        if (f == 1) {
            zuZahlenderBetrag = er * anzTickets;
        } else if (f == 2) {
            zuZahlenderBetrag = tr * anzTickets;
        } else if (f == 3) {
            zuZahlenderBetrag = ktr * anzTickets;
        }
        */

        /* A5.3: 1. Aufgabe
         * Durch die Verwaltung der Fahrkartenbezeichnung und Fahrkartenpreise mit einem Array ist die Übersicht klarer und
         * es lassen sich einfacher weitere Operationen mit den Werten durchführen, so müssen zum Beispiel keine aufwendigen switch-case Fälle geschrieben werden.
         * Es muss nicht für jeden Wert eine eigene Variable bereitgestellt werden. Es spart Zeilen.
         */
        /* A5.3: 3. Aufgabe
         * Die neue Art der Implementierung spart Code bei der Implementierung und ist somit auch bedeutend sparsamer bei zukünftigen Erweiterungen.
         * Wenn neue Werte hinzugefügt werden sollen, müssen nun die Grenzen der Arrays angepasst werden, in der alten Implementierung, wurde nur eine neue Variable mit dem entsprechenden Wert erstellt.
         * Die neue Implementierung ist auch hinsichtlich der weiteren Benutzung der Werte der Arrays effektiver.
         */
        String[] fk = {"Einzelfahrschein Berlin AB", "Einzelfahrschein BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        double[] fkp = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

        System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin aus:");
        for (int i = 0; i < fk.length; i++) {
            System.out.println(fk[i] + " [" + fkp[i] + " EUR] (" + (i+1) + ")");
        }
        int f = tastatur.nextByte();
        if (f > 3) {
            System.out.println(">>falsche Eingabe<<");
            fahrkartenbestellungErfassen();
        }
        System.out.println("Anzahl der Tickets: ");
        anzTickets = tastatur.nextByte();
        zuZahlenderBetrag = fkp[f-1] * anzTickets;

        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingezahlterGesamtbetrag = 0.0;
        double rueckgabebetrag;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("%s%.2f\n", "Noch zu Zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.println("Eingabe (mind. 5ct, höchsten 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgabebetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein(e) wird/werden ausgegeben");
        warte(250);
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        muenzeAusgeben(rueckgabebetrag, "Euro");
        System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen! Wir wünschen Ihnen eine gute Fahrt.");
    }

    private static void warte(int milliesekunde) {
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(milliesekunde);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    private static void muenzeAusgeben(double betrag, String Einheit) {
        if(betrag > 0.0)
        {
            System.out.printf("%s%.2f%s\n", "Der Rückgabebetrag in Höhe von ", betrag, " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(betrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2.00 " + Einheit);
                betrag -= 2.0;
            }
            while(betrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1.00 " + Einheit);
                betrag -= 1.0;
            }
            while(betrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("0.50 " + Einheit);
               betrag -= 0.5;
            }
            while(betrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("0.20 " + Einheit);
                betrag -= 0.2;
            }
            while(betrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("0.10 "+ Einheit);
                betrag -= 0.1;
            }
            while(betrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("0.50 " + Einheit);
                betrag -= 0.05;
            }
        }
    }

    //Main - Methode
    public static void main (String[] args) {
        run();
    }

    private static void run() {
        double zzB = fahrkartenbestellungErfassen();
        double r = fahrkartenBezahlen(zzB);
        fahrkartenAusgeben();
        rueckgeldAusgeben(r);
        System.out.println("Wollen Sie weitere Tickets bestellen? (j/n)");
        String i = tastatur.next();
        if (i.equals("j")) {
            System.exit(0);
        }
    }
}
