package Schleifen1;

import java.util.ArrayList;

/**
 * Klasse gibt die ersten 100 Primzahlen aus
 */
public class A7 {

    public static void main(String[] args) {
        for (int n = 2; n <= 100; n++) {
            if (isPrime(n)) {
                System.out.println(n);
            }
        }
    }

    private static boolean isPrime(int v) {
        for (int i = 2; i <= Math.sqrt(v); i++) {
            if (v % i == 0) {
                return false;
            }
        }
        return true;
    }
}
