package Schleifen1;

import java.util.Scanner;

public class A8 {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Wie groß soll die Seitenlänge des Quadrats sein?");
        int l = in.nextInt();
        int max = l;
        while (0 != max--) {
            System.out.print("* ");
            int width = l;
            while (2 < width--) {
                if (max == l - 1 || max == 0) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println("*");
        }
    }
}
