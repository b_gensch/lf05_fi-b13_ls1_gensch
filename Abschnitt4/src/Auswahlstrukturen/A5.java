package Auswahlstrukturen;

import java.util.Scanner;

public class A5 {

    private static Scanner in = new Scanner(System.in);

    private static void bmi() {
        System.out.println("Bitte geben Sie Ihr Geschlecht an:\n" + "m -> Männlich\n" + "w -> Weiblich");
        String g = in.next();
        System.out.println("Wie viel wiegen Sie? (in kg)");
        double w = in.nextDouble();
        System.out.println("Wie groß sind Sie? (in cm)");
        double h = in.nextDouble();
        h = h / 100;
        double bmi = w / (h*h);
        switch(g) {
            case "m":
                if (bmi < 20) {
                    System.out.println("Ihr BMI liegt bei " + bmi + ".\n Sie haben Untergewicht.");
                } else if (bmi > 25) {
                    System.out.println("Ihr BMI liegt bei " + bmi + ".\n Sie haben Übergewicht.");
                } else {
                    System.out.println("Ihr BMI liegt bei " + bmi + ".\n Sie haben Normalgewicht.");
                }
                break;
            case "w":
                if (bmi < 19) {
                    System.out.println("Ihr BMI liegt bei " + bmi + ".\n Sie haben Untergewicht.");
                } else if (bmi > 24) {
                    System.out.println("Ihr BMI liegt bei " + bmi + ".\n Sie haben Übergewicht.");
                } else {
                    System.out.println("Ihr BMI liegt bei " + bmi + ".\n Sie haben Normalgewicht.");
                }
                break;
        }
    }

    public static void main(String[] args) {
        bmi();
    }
}
