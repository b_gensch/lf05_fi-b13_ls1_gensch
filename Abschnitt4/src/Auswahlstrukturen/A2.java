package Auswahlstrukturen;

import java.util.Scanner;

public class A2 {

    private static Scanner in = new Scanner(System.in);

    private static void steuersatz() {
        double p, brutto;
        System.out.println("Nettowert eingeben: ");
        double netto = in.nextDouble();
        System.out.println("Welcher Steuersatz soll angewendet werden?\n" +
                "j -> ermaeßigter Steuersatz\n" +
                "n -> voller Steuersatz");
        String s = in.next();
        switch (s) {
            case "j":
                p = netto * 7 / 100;
                brutto = netto + p;
                System.out.println("Der Bruttobetrag mit dem ermaeßigten Steuersatz liegt bei:\n" + brutto + " Euro");
                break;
            case "n":
                p = netto * 19 / 100;
                brutto = netto + p;
                System.out.println("Der Bruttobetrag mit vollem Steuersatz liegt bei:\n" + brutto + " Euro");
        }
    }

    public static void main (String[] args) {
        steuersatz();
    }
}
