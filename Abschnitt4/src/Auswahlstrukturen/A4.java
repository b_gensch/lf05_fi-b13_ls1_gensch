package Auswahlstrukturen;

import java.util.Scanner;

public class A4 {
    private static Scanner in = new Scanner(System.in);

    private static void rabatt() {
        double p, r, e;
        System.out.println("Wie hoch ist der Bestellwert?");
        double bwert = in.nextDouble();
        p = bwert * 19 / 100;
        if (bwert <= 100) {
            r = bwert * 10 / 100;
            e = bwert + p - r;
            System.out.println("Der Bestellwert liegt inklusive der Mehrwertsteuer und 10% Rabatt bei: " + e);
        } else if (bwert <= 500) {
            r = bwert * 15 / 100;
            e = bwert + p - r;
            System.out.println("Der Bestellwert liegt inklusive der Mehrwertsteuer und 15% Rabatt bei: " + e);
        } else {
            r = bwert * 20 / 100;
            e = bwert + p - r;
            System.out.println("Der Bestellwert liegt inklusive der Mehrwertsteuer und 20% Rabatt bei: " + e);
        }
    }

    public static void main(String[] args) {
        rabatt();
    }
}
