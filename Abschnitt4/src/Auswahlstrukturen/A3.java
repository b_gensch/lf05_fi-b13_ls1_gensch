package Auswahlstrukturen;

import java.util.Scanner;

public class A3 {

    private static Scanner in = new Scanner(System.in);

    private static void mice() {
        int s = 10;
        System.out.println("Wie viel kostet eine Maus?");
        double price = in.nextDouble();
        System.out.println("Wie viele Mäuse wurden bestellt?");
        int anz = in.nextInt();
        double p = price * 19 / 100;
        price = (price + p) * anz;
        if (anz < 10) {
            price += s;
        }
        System.out.println("Die " + anz + " PC-Mäuse kosten insgesamt:\n" + price);
    }

    public static void main (String[] args) {
        mice();
    }
}
