package Auswahlstrukturen;

import java.util.Arrays;
import java.util.Scanner;

public class Sortiere_abc {

    private static final Scanner in = new Scanner(System.in);

    public static void sortArray() {
        System.out.println("Taetigen Sie bitte eine Eingabe (maximal 3 Zeichen).");
        String e = in.next();
        char[] StrToChar = e.toCharArray();
        if (StrToChar.length > 3) {
            System.out.println("Ihre Eingabe war zu lang, bitte wiederholen.");
            sortArray();
        }
        Arrays.sort(StrToChar);
        System.out.println("Ihre Eingabe wurde sortiert:");
        System.out.println(StrToChar);
    }

    public static void main(String[] args) {
        sortArray();
    }
}
