package Auswahlstrukturen;

import java.util.Scanner;

public class A6 {

    private static Scanner in = new Scanner(System.in);

    public static void funcSolver() {
        double r;
        System.out.println("Bitte geben Sie einen x Wert ein.");
        double x = in.nextDouble();
        if (x <= 0) {
            r = Math.pow(2.718, x);
            System.out.println("Der Funktionswert ist: " + r);
            System.out.println("Der Funktionswert ist exponentiell.");
        } else if (x > 3) {
            r = 2 * x + 4;
            System.out.println("Der Funktionswert ist: " + r);
            System.out.println("Der Funktionswert ist linear.");
        } else {
            r = x * x + 1;
            System.out.println("Der Funktionswert ist: " + r);
            System.out.println("Der Funktionswert ist quadratisch.");
        }
    }

    public static void main(String[] args) {
        funcSolver();
    }
}
