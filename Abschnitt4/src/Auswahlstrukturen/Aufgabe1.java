package Auswahlstrukturen;

import java.util.Objects;
import java.util.Scanner;

public class Aufgabe1 {

    Scanner in = new Scanner(System.in);

    private void erstens() {
        boolean hungry = false;
        System.out.println("Bin ich hungrig?");
        String eingabe = in.nextLine();
        if (eingabe.equals("Ja")) {
            hungry = true;
        }
        if (eingabe.equals("Nein")) {
            hungry = false;
        }
        if (hungry) {
            System.out.println("Ich sollte mir etwas zu essen holen.");
        }
    }

    private int eingabeZahl1() {
        int zahl1;
        System.out.println("Bitte geben Sie die erste Zahl ein: ");
        zahl1 = in.nextInt();
        return zahl1;
    }

    private int eingabeZahl2() {
        int zahl2;
        System.out.println("Bitte geben Sie die zweite Zahl ein: ");
        zahl2 = in.nextInt();
        return zahl2;
    }

    private int eingabeZahl3() {
        int zahl3;
        System.out.println("Bitte geben Sie die dritte Zahl ein: ");
        zahl3 = in.nextInt();
        return zahl3;
    }

    private void zweitens() {
        int zahl1, zahl2;
        zahl1 = eingabeZahl1();
        zahl2 = eingabeZahl2();
        if (zahl1 == zahl2) {
            System.out.println("Die beiden Zahlen sind identisch.");
        }
    }

    private void drittens() {
        int zahl1, zahl2;
        zahl1 = eingabeZahl1();
        zahl2 = eingabeZahl2();
        if (zahl2 > zahl1) {
            System.out.println("Die zweite Zahl ist größer als die erste Zahl.");
        }
    }

    private void viertens() {
        int zahl1, zahl2;
        zahl1 = eingabeZahl1();
        zahl2 = eingabeZahl2();
        if (zahl1 >= zahl2) {
            System.out.println("Die erste Zahl ist größer oder gleich der zweiten Zahl.");
        } else {
            System.out.println("Die erste Zahl ist kleiner als die zweite Zahl.");
        }
    }

    private void dreiZahlen() {
        int zahl1, zahl2, zahl3;
        zahl1 = eingabeZahl1();
        zahl2 = eingabeZahl2();
        zahl3 = eingabeZahl3();

        if (zahl1 > zahl2 && zahl1 > zahl3) {
            System.out.println("Die erste Zahl ist größer als die zweite und dritte Zahl.");
        }

        if (zahl3 > zahl2 || zahl3 > zahl1) {
            System.out.println("Die dritte Zahl ist größer als die erste oder als die zweite Zahl.");
        }

        if (zahl1 > zahl2 && zahl1 > zahl3) {
            System.out.println("Die erste Zahl ist die größte Zahl.");
        } else if (zahl2 > zahl1 && zahl2 > zahl3) {
            System.out.println("Die zweite Zahl ist die größte Zahl.");
        } else {
            System.out.println("Die dritte Zahl ist die größte Zahl.");
        }
    }
}
