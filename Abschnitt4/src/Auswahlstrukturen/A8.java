package Auswahlstrukturen;

import java.util.Scanner;

public class A8 {

    private static final Scanner in = new Scanner(System.in);

    public static void leapYear() {
        System.out.println("Bitte geben Sie eine Jahreszahl an:");
        int l = in.nextInt();
        if (l > -45) {
            if (l % 4 == 0) {
                if (l > 1582) {
                    if (l % 100 == 0) {
                        if (l % 400 == 0) {
                            System.out.println("Das Jahr " + l + " ist ein Schaltjahr.");
                        } else {
                            System.out.println("Das Jahr " + l + " ist kein Schaltjahr.");
                        }
                    } else {
                        System.out.println("Das Jahr " + l + " ist ein Schaltjahr");
                    }
                } else {
                    System.out.println("Das Jahr " + l + " ist ein Schaltjahr gewesen.");
                }
            } else {
                System.out.println("Das genannte Jahr ist kein Schaltjahr.");
            }
        } else {
            System.out.println("Schaltjahre gibt es erst seit dem Jahr 45 v.Chr., als Caesar sie eingeführt hat. ");
            System.out.println("Bitte erneut versuchen.");
            leapYear();
        }
    }

    public static void main(String[] args) {
        leapYear();
    }
}
