package Schleifen2;

import java.util.Scanner;

public class A8 {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
    matrix();
    }

    public static void matrix() {
        System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein:");
        int e = in.nextInt();
        int v = 0;

        if (e <= 9 && e >= 2) {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    while (v < 100) {
                        if (v == 0) {
                            System.out.print(v + " ");
                        } else {
                            if (v % 10 == 0) {
                                if (v % e == 0) {
                                    System.out.println();
                                    System.out.print("* ");
                                } else if (Integer.toString(v).contains(Integer.toString(e))) {
                                    System.out.println();
                                    System.out.print("* ");
                                } else if (getChecksum(v) == getChecksum(e)) {
                                    System.out.println();
                                    System.out.print("* ");
                                } else {
                                    System.out.println();
                                    System.out.print(v + " ");
                                }
                            } else {
                                if (v % e == 0) {
                                    System.out.print("* ");
                                } else if (Integer.toString(v).contains(Integer.toString(e))) {
                                    System.out.print("* ");
                                } else if (getChecksum(v) == getChecksum(e)) {
                                    System.out.print("* ");
                                } else {
                                    System.out.print(v + " ");
                                }
                            }
                        }
                        v++;
                    }
                }
            }
        } else {
            System.out.println("Ihre Eingabe war falsch.");
            matrix();
        }
    }

    private static int getChecksum(int a) {
        int cs;
        if (a < 10) {
            cs = a;
            return cs;
        } else {
            cs = a % 10 + getChecksum(a/10);
            return cs;
        }
    }
}
