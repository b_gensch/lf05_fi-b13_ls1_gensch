package Schleifen2;

import java.util.Scanner;

public class A6 {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        String a = "";
        while (!a.equals("n")) {
            System.out.println("Bitte geben Sie eine einmalige Einlage ein:");
            double e = in.nextDouble();
            System.out.println("Bitte geben Sie einen Zinssatz an:");
            double z = in.nextDouble();

            double m = 0;
            int j = 0;
            while (m < 1000000) {
                m = e + getPercent(e, z);
                e = m;
                j++;
            }

            System.out.println("Nach " + j + " Jahren sind Sie Millionär.");

            System.out.println("Wollen Sie erneut Eingaben tätigen? (j/n)");
            a = in.next();
        }
    }

    private static double getPercent(double e, double z) {
        double p;
        p = (e*z) / 100;
        return p;
    }
}
