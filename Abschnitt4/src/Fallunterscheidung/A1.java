package Fallunterscheidung;

import java.util.Scanner;

public class A1 {

    private static final Scanner in = new Scanner(System.in);

    public static void notes() {
        System.out.println("Geben Sie bitte eine Note ein:");
        byte n = in.nextByte();
        switch (n) {
            case 1:
                System.out.println("Sehr gut");
                break;
            case 2:
                System.out.println("Gut");
                break;
            case 3:
                System.out.println("Befriedigend");
                break;
            case 4:
                System.out.println("Ausreichend");
                break;
            case 5:
                System.out.println("Mangelhaft");
                break;
            case 6:
                System.out.println("Ungenügend");
                break;
        }
    }

    public static void main(String[] args) {
        notes();
    }
}
