package Fallunterscheidung;

import java.util.Scanner;

public class A5 {

    private static Scanner in = new Scanner(System.in);

    private static void ohm() {
        double r, u, i;
        System.out.println("Welche Größe soll berechnet werden?");
        String e = in.next();
        switch (e) {
            case "R":
                System.out.println("Spannung (U in Ohm):");
                u = in.nextDouble();
                System.out.println("Stromstärke (I in Ampere):");
                i = in.nextDouble();
                r = u / i;
                System.out.println("Der Widerstand beträgt: " + r + " Ohm");
                break;
            case "U":
                System.out.println("Stromstärke (I in Ampere):");
                i = in.nextDouble();
                System.out.println("Widerstand (R in Ohm):");
                r = in.nextDouble();
                u = r * i;
                System.out.println("Die Spannung beträgt: " + u + " V");
                break;
            case "I":
                System.out.println("Spannung (U in Volt):");
                u = in.nextDouble();
                System.out.println("Widerstand (R in Ohm):");
                r = in.nextDouble();
                i = u / r;
                System.out.println("Die Stromstärke beträgt: " + i + " A");
                break;
            default:
                System.out.println("Das ist leider keine Größe des Ohmschen Gesetzes, bitte versuchen Sie es erneut.");
                ohm();
                break;
        }
    }

    public static void main(String[] args) {
        ohm();
    }
}
