package Fallunterscheidung;

import java.util.HashMap;
import java.util.Scanner;

public class Rom {

    private static final Scanner in = new Scanner(System.in);

    private static HashMap<Character, Integer> romNum = new HashMap<>();

    private static void calc() {
        romNum.put('I', 1);
        romNum.put('V', 5);
        romNum.put('X', 10);
        romNum.put('L', 50);
        romNum.put('C', 100);
        romNum.put('D', 500);
        romNum.put('M', 1000);
        int result = 0;

        System.out.println("Bitte geben Sie eine römische Zahl ein (nicht größer als 3999):");
        String input = in.next();
        char[] inArray = input.toCharArray();

        for (int i = 0; i < inArray.length; i++) {
            int v1 = romNum.get(inArray[i]);
            if (i + 1 < inArray.length) {
                int v2 = romNum.get(inArray[i + 1]);
                if (v2 <= v1) {
                    result = result + v1;
                } else {
                    result = result + (v2 - v1);
                    i++;
                }
            } else {
                int v2 = romNum.get(inArray[i - 1]);
                if (v2 < v1) {
                    result = result + (v1 - v2);
                } else {
                    result = result + v1;
                }
            }
        }
        if (result < 4000) {
            System.out.println("Die römische Zahl lautet als Dezimalzahl:\n" + result);
        }
    }

    public static void main(String[] args) {
        calc();
    }
}
