package Fallunterscheidung;

import java.util.Scanner;

public class A2 {

    private static Scanner in = new Scanner(System.in);

    public static void months() {
        System.out.println("Geben Sie eine Zahl von 1 bis 12 ein.");
        byte m = in.nextByte();
        switch (m) {
            case 1:
                System.out.println("Januar");
                break;
            case 2:
                System.out.println("Februar");
                break;
            case 3:
                System.out.println("Maerz");
                break;
            case 4:
                System.out.println("April");
                break;
            case 5:
                System.out.println("Mai");
                break;
            case 6:
                System.out.println("Juni");
                break;
            case 7:
                System.out.println("Juli");
                break;
            case 8:
                System.out.println("August");
                break;
            case 9:
                System.out.println("September");
                break;
            case 10:
                System.out.println("Oktober");
                break;
            case 11:
                System.out.println("November");
                break;
            case 12:
                System.out.println("Dezember");
                break;
        }
    }

    public static void main(String[] args) {
        months();
    }
}
