package Fallunterscheidung;

import java.util.Scanner;

public class A4 {

    private static Scanner in = new Scanner(System.in);

    private static void calc() {
        double res;
        System.out.println("Geben Sie die erste Zahl ein:");
        double z1 = in.nextDouble();
        System.out.println("Geben Sie die zweite Zahl ein.");
        double z2 = in.nextDouble();
        System.out.println("Waehlen Sie bitte eine Rechenoperation. (+, -, *, /)");
        String r = in.next();
        switch (r) {
            case "+":
                res = z1 + z2;
                System.out.println(res);
                break;
            case "-":
                res = z1 - z2;
                System.out.println(res);
                break;
            case "*":
                res = z1 * z2;
                System.out.println(res);
                break;
            case "/":
                res = z1 / z2;
                System.out.println(res);
                break;
            default:
                System.out.println("Sie haben leider keine gültige Rechenoperation angegeben, beginnen Sie erneut.");
                calc();
                break;
        }
    }

    public static void main(String[] args) {
        calc();
    }
}
