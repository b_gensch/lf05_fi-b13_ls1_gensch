package Fallunterscheidung;

import java.util.Scanner;

public class A3 {

    private static Scanner in = new Scanner(System.in);

    private static void rome() {
        System.out.println("Geben Sie eine römische Zahl ein.");
        String r = in.next();
        switch (r) {
            case "I":
                System.out.println("1");
                break;
            case "V":
                System.out.println("5");
                break;
            case "X":
                System.out.println("10");
                break;
            case "L":
                System.out.println("50");
                break;
            case "C":
                System.out.println("100");
                break;
            case "D":
                System.out.println("500");
                break;
            case "M":
                System.out.println("1000");
                break;
            default:
                System.out.println("Sie haben leider keine roemische Zahl eingegeben, versuchen Sie es bitte erneut.");
                rome();
                break;
        }
    }

    public static void main(String[] args) {
        rome();
    }
}
