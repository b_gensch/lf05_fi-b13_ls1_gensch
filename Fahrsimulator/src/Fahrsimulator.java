import java.util.Scanner;

public class Fahrsimulator {

    private static Scanner myScanner = new Scanner(System.in);

    public static void main(String[] args) {
        double v = 0;
        int i = 1;
        fahrt(v, i);
    }

    public static void fahrt(double v, int i) {

        while (i == 1) {
            System.out.println("Was wollen Sie tun?");
            System.out.println("1: Geschwindigkeit ändern");
            System.out.println("0: Fahrt beenden");
            System.out.println("Bitte einen Menüpunkt wählen:");
            i = myScanner.nextInt();
            if (i == 1) {
                System.out.println("Bitte geben Sie die Änderung der Geschwindigkeit an (in km/h)");
                double dv = myScanner.nextInt();
                if (beschleunige(v, dv) > 130) {
                    System.out.println("Schneller können Sie leider nicht fahren, bitte erneut versuchen.");
                    fahrt(v, i);
                } else if (beschleunige(v, dv) < 0) {
                    System.out.println("Langsamer fahren als stehen können Sie nicht, bitte erneut versuchen.");
                    fahrt(v, i);
                } else {
                    v = beschleunige(v, dv);
                    System.out.println("Sie fahren " + v + " km/h.");
                }
            }
        }
        System.exit(0);
    }

    static double beschleunige(double v, double dv) {
        v = v + dv;
        return v;
    }
}