import java.util.Scanner;

public class PCHaendler {

    static Scanner myScanner = new Scanner(System.in);

    public static void main(String[] args) {
        // Benutzereingaben lesen
        System.out.println("was m�chten Sie bestellen?");
        String artikel = liesString();

        System.out.println("Geben Sie die Anzahl ein:");
        int anzahl = liesInt();

        System.out.println("Geben Sie den Nettopreis ein:");
        double preis = liesDouble();

        System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
        double mwst = liesDouble();

        // Verarbeiten
        double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
        double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

        // Ausgeben
        rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
    }

    public static String liesString () {
        String s;
        s = myScanner.next();
        return s;
    }

    public static int liesInt () {
        int i;
        i = myScanner.nextInt();
        return i;
    }

    public static double liesDouble () {
        double d;
        d = myScanner.nextDouble();
        return d;
    }

    public static double berechneGesamtnettopreis (int anz, double nettopreis) {
        double d;
        d = anz * nettopreis;
        return d;
    }

    public static double berechneGesamtbruttopreis (double nettogesamtpreis, double mwst) {
        double d;
        d = nettogesamtpreis * (1 + mwst / 100);
        return d;
    }

    public static void rechnungAusgeben (String art, int anz, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto: %-20s %6d %10.2f %n", art, anz, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", art, anz, bruttogesamtpreis, mwst, "%");
    }
}
