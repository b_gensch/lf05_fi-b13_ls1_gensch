package ArrayAufgabenMitMethoden;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayHelper {

    //Aufgabe 1
    public static String convertArrayToString(int[] zahlen) {
        String array;
        array = Arrays.toString(zahlen);
        return array;
    }

    public static void main(String[] args) {
        int[][] array = get2DArray(3, 3);
        System.out.println(isTransposed(array));
    }

    //Aufgabe 2
    public static int[] swapNumbers(int[] zahlen) {
        int temp;

        for (int i = 0; i < (zahlen.length / 2); i++) {
            temp = zahlen[i];
            zahlen[i] = zahlen[(zahlen.length - 1) - i];
            zahlen[(zahlen.length - 1) - i] = temp;
        }

        return zahlen;
    }

    //Aufgabe 3
    public static int[] swapNumbersNewField(int[] zahlen) {
        int[] z = new int[zahlen.length];

        for (int i = 0; i < zahlen.length; i++) {
            z[i] = zahlen[(zahlen.length - 1) - i];
        }

        return z;
    }

    //Aufgabe 4
    public static void temperatureTable(int rows) {
        double [][] tempTable = new double[2][rows];

        for (int j = 0; j < rows; j++) {
            tempTable[0][j] = j * 10;
        }
        for (int i = 0; i < rows; i++) {
            double c = (tempTable[0][i] - 32.0) * (5.0 / 9.0);
            tempTable[1][i] = c;
        }
    }

    //Aufgabe 5
    public static int[][] get2DArray(int x, int y) {
        Scanner input = new Scanner(System.in);
        int[][] array = new int[x][y];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.println("Geben Sie den Wert für [" + i + "][" + j + "] ein:");
                int h = input.nextInt();
                array[i][j] = h;
            }
        }

        return array;
    }

    /*Aufgabe 6
    only works with quadratic arrays */
    public static boolean isTransposed(int[][] array) {
        boolean r = false;
        int[][] temp = new int[array.length][array[0].length];

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                temp[j][i] = array[i][j];
            }
        }

        for (int i = 0; i < array.length; i++) {
            r = (temp[i][i] == array[i][i]);
        }

        return r;
    }
}
