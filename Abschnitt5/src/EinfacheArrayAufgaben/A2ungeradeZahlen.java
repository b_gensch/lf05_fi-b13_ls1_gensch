package EinfacheArrayAufgaben;

public class A2ungeradeZahlen {

    public static void main(String[] args) {
        int[] ungeradeZahlen = new int[10];
        for (int i = 0; i < ungeradeZahlen.length; i++) {
            ungeradeZahlen[i] = i * 2 + 1;
        }
        for (int j : ungeradeZahlen) {
            System.out.println(j);
        }
    }
}
