package EinfacheArrayAufgaben;

public class A4Lotto {

    private static int[] l = {3, 7, 12, 18, 37, 42};

    private static void a() {
        System.out.print("[ ");
        for (int j : l) {
            System.out.print(j + " ");
        }
        System.out.print(" ]");
    }

    private static void b() {
        boolean enthaelt12 = false, enthaelt13 = false;
        for (int i = 0; i < l.length; i++) {
            if (l[i] == 12) {
                enthaelt12 = true;
            }
            if (l[i] == 13) {
                enthaelt13 = true;
            }
        }
        if (enthaelt12 == true) {
            System.out.println("\nDie Zahl 12 ist in der Ziehung enthalten.");
        } else {
            System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
        }
        if (enthaelt13 == true) {
            System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
        } else {
            System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
        }
    }

    public static void main(String[] args) {
        a();
        b();
    }
}
