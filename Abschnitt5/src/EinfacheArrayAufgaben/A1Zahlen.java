package EinfacheArrayAufgaben;

public class A1Zahlen {

    public static void main(String[] args) {
        int[] zahlen = new int[10];
        for (int i = 0; i < zahlen.length; i++) {
            zahlen[i] = i;
        }
        for (int j : zahlen) {
            System.out.println(j);
        }
    }
}
