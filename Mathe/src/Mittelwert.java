import java.util.ArrayList;
import java.util.Scanner;

public class Mittelwert {

    private static Scanner in = new Scanner(System.in);

    private static ArrayList<Float> values = new ArrayList<>();

    private static void calc() {
        String ein;
        float e, h = 0;
        do {
            System.out.println("Bitte geben Sie einen Wert ein:");
            e = in.nextFloat();
            values.add(e);
            System.out.println("Wollen Sie einen weiteren Wert eingeben? (y/n)");
            ein = in.next();
        } while (ein.equals("y"));
        System.out.println("Der Mittelwert wird berechnet...");
        for (Float value : values) {
            h += value;
        }
        float m = h / values.size();
        System.out.println("Eingegebene Werte: " + values);
        System.out.println("Der Mittelwert lautet: " + m);
    }

    public static void main(String[] args) {
        calc();
    }
}
