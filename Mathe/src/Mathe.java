public class Mathe {

    public double berechneMittelwert (double x, double y) {
        double m;

        m = (x + y) / 2.0;

        return m;
    }

    public double reihenschaltung (double r1, double r2) {
        double rges;
        rges = r1 + r2;
        return rges;
    }

    public double parallelschaltung (double r1, double r2) {
        double pges;
        pges = 1.0 / (1.0 / r1 + 1.0 / r2);
        return pges;
    }

    public double quadrat (double x) {
        double q;
        q = x * x;
        return q;
    }

    public double hypotenuse (double kathete1, double kathete2) {
        double h = Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
        return h;
    }

    public double quadrieren (double value) {
        double p = value * value;
        return p;
    }
}