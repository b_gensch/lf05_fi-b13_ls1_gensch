public class Aufgabe1 {

    /*
    Der Unterschied zwischen print und println ist, dass println jede Ausgabe in einer neuen Zeile ausgibt,
    während print in der gleichen Zeile weiterschreibt.
     */

    public static void main (String[] args) {
        System.out.println("Diese Aufgaben sind gar nicht mal so schwer.");
        System.out.println("Diese Aufgabe ist überhaupt nicht schwer");

        String a = "Diese Aufgabe", b = "ist überhaupt", c = "nicht schwer";
        System.out.println("Diese \"Aufgabe\" ist gar nicht mal\nso schwer");
        System.out.println(a + " " + b + " " + c);
    }
}
