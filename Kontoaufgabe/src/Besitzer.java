public class Besitzer {

    //Attribute
    private String name, surname;
    private Konto k1, k2;

    //Konstruktor
    public Besitzer(String n, String sn) {
        this.name = n;
        this.surname = sn;
    }

    //Getter Methoden
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Konto getK1() {
        return k1;
    }

    public Konto getK2() {
        return k2;
    }

    //Setter-Methoden
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setK1(Konto k1) {
        this.k1 = k1;
    }

    public void setK2(Konto k2) {
        this.k2 = k2;
    }

    //Methoden
    public void gesamtUebersicht() {
        System.out.println("1. Konto: " + k1.getIban() + ", " + k1.getKnr() + "; " + k1.getGeld());
        System.out.println("2. Konto: " + k2.getIban() + ", " + k2.getKnr() + "; " + k2.getGeld());
    }

    public void gesamtGeld() {
        float gV = k1.getGeld() + k2.getGeld();
        System.out.println("Gesamtvermögen: " + gV + " Euro");
    }
}
