public class Konto {

    //Attribute
    private String iban;
    private long knr;
    private float geld;

    //Konstruktor
    public Konto(String i, long nr) {
        this.iban = i;
        this.knr = nr;
    }

    //Getter Methoden
    public String getIban() {
        return iban;
    }

    public long getKnr() {
        return knr;
    }

    public float getGeld() {
        return geld;
    }

    //Setter Methoden
    public void setIban(String iban) {
        this.iban = iban;
    }

    public void setKnr(long knr) {
        this.knr = knr;
    }

    public void setGeld(float geld) {
        this.geld = geld;
    }

    //Methoden
    public void abheben(float a) {
        float h = geld;
        geld = h - a;
        System.out.println("Es wurden " + a + " Euro abgehoben.");
    }

    public void einzahlen(float e) {
        float h = geld;
        geld = h + e;
        System.out.println("Es wurden " + e + " Euro eingezahlt.");
    }

    public void ueberweisen(Konto ziel, float betrag) {
        geld = geld - betrag;
        ziel.einzahlen(betrag);
        System.out.println("Es wurden " + betrag + " Euro an " + ziel.iban + " überwiesen");
    }
}
