import org.junit.Test;
import static org.junit.Assert.*;

public class KontoTest {

    Konto test1 = new Konto("DE90123456781234567890", 123456789);
    Konto test2 = new Konto("DE90876543211234567089", 1234567089);
    Besitzer btest1 = new Besitzer("Benjamin", "Gensch");

    @Test
    public void testAbheben() {
        test1.setGeld(5000);
        test1.abheben(500);
        assertEquals(4500, test1.getGeld(), 0);
    }

    @Test
    public void testEinzahlen() {
        test1.setGeld(5000);
        test1.einzahlen(500);
        assertEquals(5500, test1.getGeld(), 0);
    }

    @Test
    public void testUeberweisen() {
        test1.setGeld(4000);
        test2.setGeld(0);
        test1.ueberweisen(test2, 3500);
        assertEquals(500, test1.getGeld(), 0);
        assertEquals(3500, test2.getGeld(), 0);
    }

    @Test
    public void testBesitzerUebersicht() {
        test1.setGeld(5000);
        test2.setGeld(2000);
        btest1.setK1(test1);
        btest1.setK2(test2);

        //Test Konto 1
        assertEquals("DE90123456781234567890", btest1.getK1().getIban());
        assertEquals(123456789, btest1.getK1().getKnr());
        assertEquals(5000, btest1.getK1().getGeld(), 0);

        //Test Konto 2
        assertEquals("DE90876543211234567089", btest1.getK2().getIban());
        assertEquals(1234567089, btest1.getK2().getKnr());
        assertEquals(2000, btest1.getK2().getGeld(), 0);
    }

    @Test
    public void testBesitzerGeld() {
        test1.setGeld(5000);
        test2.setGeld(2000);
        btest1.setK1(test1);
        btest1.setK2(test2);

        float actual = btest1.getK1().getGeld() + btest1.getK2().getGeld();
        assertEquals(7000, actual, 0);
    }
}