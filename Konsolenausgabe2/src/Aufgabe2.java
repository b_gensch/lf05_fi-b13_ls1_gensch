public class Aufgabe2 {

    public static void main (String[] args) {

        int a = 1, b = 2, c = 3, d = 4, e = 5, f = 0;
        int x, v, z, u, o;

        x = factcalc(f);
        v = factcalc(b);
        z = factcalc(c);
        u = factcalc(d);
        o = factcalc(e);

        System.out.printf("%-5s = %-18s = %4d\n", factString(f), "", x);
        System.out.printf("%-5s = %-18s = %4d\n", factString(a), "1", x);
        System.out.printf("%-5s = %-18s = %4d\n", factString(b), "1 * 2", v);
        System.out.printf("%-5s = %-18s = %4d\n", factString(c), "1 * 2 * 3", z);
        System.out.printf("%-5s = %-18s = %4d\n", factString(d), "1 * 2 * 3 * 4", u);
        System.out.printf("%-5s = %-18s = %4d\n", factString(e), "1 * 2 * 3 * 4 * 5", o);
    }

    public static int factcalc(int s) {
        int r = 1;
        for (int i = 2; i <= s; i++) {
            r = r * i;
        }
        return r;
    }

    public static String factString(int s) {
        String r;
        r = s + "!";
        return r;
    }
}
